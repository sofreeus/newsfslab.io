# Software Freedom School

![Software Freedom School logo](img/clean.jpeg)

## What we're about

Teaching the world how to use and why to choose free software.

SFS has been teaching FLOSS (Free/Libre and Open Source Software) since before 2013.

In 2013, we delivered our first Linux Camp for sysadmins, four days of learning by immersion.

In 2018, we delivered our first DevOps Camp for developers and sysadmins specializing in DevOps tech, like CI/CD, cloud computing, containerization, and orchestration.

We developed SFS Method, a teaching framework to enable quality technical training by passionate SME's who are not expert teachers.

We cater to Subject Matter Experts (SME's) and newbies alike - any technology professional is very often both, being one or the other at different times, sometimes even within the same subject matter. Providing the environment to move from one role to the other is where the Software Freedom School truly shines!

We honor by emulation, the spirit of the Free Software Foundation: "When we speak of free software, we are referring to freedom, not price." This means that we, like the FSF, are fully committed to the user’s Four Freedoms: the freedoms to use, study, share, and modify the software. In our case, the defend the learner's freedom to use, study, share, and modify our training material.

We believe that the use and creation of software and is both an art and a science, and we believe that art and science progress best when the freedom of creators and users is protected.

In that spirit, SFS teaches free software in a free software way. The assets we create or commission are all given to our students under a CC-BY-SA copyleft license. You are allowed and encouraged to use, study, share, modify our materials, and even to make money at it if you have the opportunity. We only ask that you give us credit and respect your students' freedom just as we respect yours.

Please remember:

* Enjoy the experience. Do what you can to make it better for everyone.
* Take care of the materials and the space. Remember to "reduce, reuse, recycle, restore, & rethink." We provide the tools for you to freely use!
* Maintain a beginner's mindset. Keep your mind open to learning new things, even if you consider yourself an expert on a particular topic or subject matter.
* Take turns with the expert and novice roles. Have confidence in your answer when in the expert role. If you must touch another person's keyboard to solve a problem, revert your changes and let them try it. We believe the best learning happens through direct experience. On the other hand, don't hesitate to ask others when you don't understand everything or are having trouble keeping up with the material.
* Don't distract others. Use Mattermost if you would like to pass notes. When you're stuck, ask your neighbor for help first, then ask the TA (Teacher's Assistant). And please, keep your clothes on.
* Pay for the class. Almost every SFS class is PWYC (Pay What You Choose). You know what your budget is and what your values are. Whether you pay what was suggested, more, less, or differently, is up to you. You can pay with cash in the Give Jar, use the Envelopes of Karmic Justice, PayPal, Bitcoin, Litecoin...

## Main phone number

720-333-5267

## HQ Location

4237 W Grand Avenue

Littleton, Colorado  80123

<iframe width="600" height="450" src="https://www.openstreetmap.org/export/embed.html?bbox=-105.04247456789018%2C39.62880647365857%2C-105.04065066576005%2C39.63139282910131&amp;layer=mapnik" style="border: 1px solid black"></iframe>
<br />
<small><a href="https://www.openstreetmap.org/#map=19/39.630100/-105.041563">View Larger Map</a></small>

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Please open an Issue or Merge Request in the [GitLab Project which creates this website](https://gitlab.com/sofreeus/newsfs.gitlab.io) if you have problems or suggestions.
