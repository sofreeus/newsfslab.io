# Software Freedom School

![Software Freedom School logo](img/clean.jpeg)

### New Page First Header

New Page First Paragraph

### New Page Second Header

New Page Second Paragraph

New Page Third Paragraph

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Please open an Issue or Merge Request in the [GitLab Project which creates this website](https://gitlab.com/sofreeus/newsfs.gitlab.io) if you have problems or suggestions.
