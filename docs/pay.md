# Software Freedom School

![Software Freedom School logo](img/clean.jpeg)

## Contribute Monthly

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
  <input type="hidden" name="cmd" value="_s-xclick" />
  <input type="hidden" name="hosted_button_id" value="387FCEZ27A9VL" />
  <table>
    <tr>
      <td>
        <input type="hidden" name="on0" value="Contributor Levels"/>
        Contributor Levels
      </td>
    </tr>
    <tr>
      <td>
        <select name="os0">
          <option value="Because you think we&#39;re cool">
            Because you think we&#39;re cool $3.14 USD - month(s)
          </option>
          <option value="the Gathering">
            the Gathering $16.00 USD - month(s)
          </option>
          <option value="SFS Third Saturday">
            SFS Third Saturday $32.00 USD - month(s)
          </option>
          <option value="SFS ROCKS">
            SFS ROCKS $64.00 USD - month(s)
          </option>
        </select>
      </td>
    </tr>
  </table>
  <input type="hidden" name="currency_code" value="USD" />
  <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribe_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Subscribe" />
  <!--
   <input type="image" src="img/commit.png" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Commit" />
  -->
</form>

## SFS: How To Pay         

How to pay for an SFS event
===========================

* [Bitcoin](/images/bitcoin.png):125TaTUWAzozYK8dWmXXvAzCsBr7eCWZUP
* [Litecoin](/images/litecoin.png):LR6WNuNZ4bH18RbHqczkde8zrBG6MTMHoo
* Put it in the Give Jar
* [PayPal](https://paypal.me/sfs303)
* Take an Envelope of Karmic Justice
* Reach out to [pwyc@sofree.us](mailto:pwyc@sofree.us) with questions

**Scan to pay for the SFS Gathering (Small is Beautiful)**

![SFS Gathering Payment](img/Gathering-qrcode.png)


PWYC is Pay What You Choose
===========================

Some folks would rather pay more, less, or differently than what we've asked. If you're reading this, you're probably one of them. For you, we have Pay What You Choose. You have all the freedom and responsibility now.

You Want to Pay More
--------------------

You love everything about SFS, and you want take make sure that our cupboards overflow. Thanks. We love you, too. We will gratefully recieve your generous gift at any of the usual places.

You Want to Pay Less
--------------------

You've never been to an SFS event, so you're not sure it'll be worth all that, so you'd like to pay some now and some later, if it turns out to be worth it. That's fine. We're happy to earn it. Come see.

You don't think the class is worth what we're asking. OK. We're cool with that, too. You pay what *you* think it's worth, but please let us know how we can improve, how we can live up to our high opinion of ourselves, so that we can be worth what we think we are.

You Want to Pay Differently
---------------------------

You don't have much USD. That's cool. We take all kinds of money. If it's money, we take it. Crypto, metals, foreign money, ancient money, stocks, bonds, you name it, we take it. The only thing we don't take is livestock. (gross exaggeration)

You don't have much money. That's cool. We take gifts of talent and time, too. Teach a class for us. Make us a llama painting. Help with writing, maintaining, and testing training material. And, thank you for your non-monetary trade. Often, these gifts mean more to us than money can.

You'd rather Pay It Forward. That's cool. Go volunteer at the local homeless shelter or animal rescue. Send us a picture postcard. We'll treasure it.

Choose.

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Please open an Issue or Merge Request in the [GitLab Project which creates this website](https://gitlab.com/sofreeus/newsfs.gitlab.io) if you have problems or suggestions.
