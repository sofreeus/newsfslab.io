# Software Freedom School

![Software Freedom School logo](img/clean.jpeg)

## Some of our favorite places

### Near Us

*   [BLUG](https://lug.boulder.co.us/) Boulder Linux User Group
*   [Code](https://www.code-talent.com/) Code Tealent
*   [denhac](https://denhac.org/) The Denver Hackerspace
*   [Rule4](https://www.rule4.com/) Rule4
*   [TinkerMill](https://www.tinkermill.org/) The Longmont Makerspace

### On the Internet

*   [HPR](https://hackerpublicradio.org) Hacker Public Radio
*   [FSF](https://fsf.org) The Free Software Foundation
*   [SDF](https://sdf.org) SDF Public Access UNIX System

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Please open an Issue or Merge Request in the [GitLab Project which creates this website](https://gitlab.com/sofreeus/newsfs.gitlab.io) if you have problems or suggestions.
