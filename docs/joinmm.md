# Software Freedom School

![Software Freedom School logo](img/clean.jpeg)

## How to join SFS Mattermost

Browse to [https://mattermost.sofree.us](https://mattermost.sofree.us)

To register and authenticate with GitLab.com, click "GitLab"

If you'd rather register with your email address, see the next section.

![screen](img/join-mm-screen-1.png)

Click "Authorize"

![screen](img/join-mm-screen-2.png)

Click "SFS303"

![screen](img/join-mm-screen-3.png)

Click "Next" or "Skip tutorial"

![screen](img/join-mm-screen-4.png)

Optionally, DM somebody.

*   dlwillson, the creator of these terrific instructions

![screen](img/join-mm-screen-5.png)

## How to join SFS Mattermost by email

Browse to [https://mattermost.sofree.us](https://mattermost.sofree.us)

Click "Create one now."

![screen](img/join-mm-screen-e1.png)

Click "Email and Password"

![screen](img/join-mm-screen-e2.png)

Fill out the form with your email address, username, and password.

![screen](img/join-mm-screen-e3.png)

Verify your email address by clicking the link in the email message you get.

(varies)

Click "Sign in"

![screen](img/join-mm-screen-e4.png)

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Please open an Issue or Merge Request in the [GitLab Project which creates this website](https://gitlab.com/sofreeus/newsfs.gitlab.io) if you have problems or suggestions.
