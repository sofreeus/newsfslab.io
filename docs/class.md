# Software Freedom School

![Software Freedom School logo](img/clean.jpeg)

## SFS Method (teach-the-teacher)

This course gives a Subject-Matter Expert (SME) a framework for delivery of technical training that will help all sorts of learners to both learn and enjoy the experience. The Time Pie, or Talk, Demo, Pair and Share is required for SME's who would like to teach with SFS.

## Study Groups

Study groups are typically 8 or 16 weeks long and 2 or 4 hours each week. We've done study groups for RHCE, Python, GNU/Linux System Administration, Puppet, and Bash Programming.

## Python Programming

This introduction to Python teaches the basics of programming.

## Kubernetes

Container Management via Kubernetes. Another of our introductory classes, this one teaches the students how to use the Kubernetes software package.

## Ansible

Taught by several different SFS teachers, Ansible is a fantastic way to manage servers and clusters. This class is usually targeted for beginners but we have the ability to offer intermediate and advanced Ansible classes.

## Cryptocurrency

High level overview of how it works and practice using it and why some cryptocurrency may be better money than most/all fiat.

## Bash

Bash programming basics. We've taught bash in several different ways and forms from several different experienced teachers. If you'd like to request a Bash class, we can teach to all levels of students in several formats.

## Security: Penetrations and Remediations

Taught annually by one of our favorite SFS partners, Penetrations and Remediations is a class for students of all levels. Teaching the student how to effectively defend their systems against outside threats makes for an excellent class.

## OpenTofu, formerly Terraform

Infrastructure as code. OpenTofu is a great way to manage your infrastructure. This class is taught to the beginner level using Docker, OpenStack, and Proxmox as IaaS platforms.

## Docker

Another of our more popular classes, Docker is a great way to manage and use containers in your infrastructure. This class can be taught at all levels.

## SQL

This introduction to SQL teaches the student the basics of using SQL statements to search and manage data in SQL databases.

## GNU/Linux

GNU/Linux is the cornerstone of the Internet these days. We teach GNU/Linux in several different forms, from 4 day bootcamps to weekly study groups. Again, we can teach this class to several different types of experience levels.

## Continuous Integration

Learn CI (continuous integration) with GitLab! This is one of our more popular classes, teaches end users how to set up a continuous delivery pipeline using GitLab.

## Intro to DevOps

This full-day class teaches the basics of DevOps. From GNU/Linux to cloud compute, GitLab to containers, all of the basic components of DevOps are included in a wholistic, end-to-end software delivery pipeline.

## Git and Markdown

Git is a simple way to manage your code. The basics of Git and the Markdown utility are all taught in a fun 4 hour class.

## Nextcloud, formerly ownCloud

Have you ever wanted to host your own Dropbox / Google Drive? With Nextcloud and ownCloud you can do just that! Teaching the basics of setting up your own cloud storage server.

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Please open an Issue or Merge Request in the [GitLab Project which creates this website](https://gitlab.com/sofreeus/newsfs.gitlab.io) if you have problems or suggestions.
