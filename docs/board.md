# Software Freedom School

![Software Freedom School logo](img/clean.jpeg)

### The Core SFS Team

| Name | Contact Info | About |
|-----|-----|-----|
| _David L. Willson_ | dlwillson (at) sofree (dot) us | The starry-eyed pirate captain who turned the twinkle in his eye into the Software Freedom School in 2013. David has nearly three decades of experience as a Linux sysadmin and has appeared across the continent at events supporting free software. |
| _Heather_ | hlwillson (at) sofree (dot) us  | It is good to be David's wife and co-founder of SFS. There are many things I contribute to SFS but my favorite is feeding the geeks. It is a pleasure to serve my friends and be in a community that truly takes care of and supports one another. |
| _Aaron_ | aayore (at) sofree (dot) us  | Aaron is part of the original crew of SFS. He brings DevOps, leadership, and a desire to make our community great! |

SFS wants to thank the former board members and long-term contributors without whom we could not have come so far: Mar Williams, Dave Anselmi, Jeffrey S. Haemer, Gary Romero, Troy Ridgley, Silvia, Anthony Sayre, Alex Wise, Ed Schaefer, Michael Shoup, , Marcus Holtz ... and so many others.

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Please open an Issue or Merge Request in the [GitLab Project which creates this website](https://gitlab.com/sofreeus/newsfs.gitlab.io) if you have problems or suggestions.
